/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 
 
 /**
 * @author Burdet Nicolas <burdet@b35pc23.ecole.ensicaen.fr>
 * @version 0.0.1 / 29-05-2018
 */

/**
 * @file simulation.c
 */
 
#include <epidemie.h>

void initsimulation () {
	pthread_t citoyen[35];
	
	coord coordonnee;
	
	struct Ville *memoire;
	int descripteur_memoire;
	int taille_memoire;
	
	taille_memoire = (1 * sizeof(struct Ville));
   
	signal(SIGINT, traiter_sigint); /* préférez la version POSIX vue en cours ←-*/

	/* - 1 - crée le segment de mémoire partagée comme un fichier */
	descripteur_memoire = shm_open(CHEMIN_MEMOIRE, O_RDWR, S_IRUSR|S_IWUSR);

	if (descripteur_memoire < 0) {
		traiter_erreur("Erreur lors de l'appel à shm_open\n");
	}

	/* - 3 - projette le segment de mémoire partagée dans l'espace mémoire du ←-processus */
	memoire = (struct memoire_t *) mmap(NULL,taille_memoire,PROT_READ,MAP_SHARED,descripteur_memoire,0);

	if (memoire == MAP_FAILED) {
		traiter_erreur("Erreur lors de l'appel à mmap\n");;
	}
	
	//Creation et placement des medecins
	printf(" Creation du medecin %d \n", 1);
	coordonnee.x=3;
	coordonnee.y=3;
	pthread_create(&citoyen[0], NULL, initMedecin, &coordonnee);
	
	for(int i=1; i<3; i++) {
		printf(" Creation du medecin %d\n", i);
		coordonnee.x=rand()%7;
		coordonnee.y=rand()%7;
		pthread_create(&citoyen[i], NULL, initMedecin, &coordonnee);
	}
	
	//Gestion des pompier, utilisation thread
	for(int i=3; i<9; i++) {
		printf(" Creation du pompier %d\n", i-4);
		coordonnee.x=rand()%7;
		coordonnee.y=rand()%7;
		pthread_create(&citoyen[i], NULL, initPompier, &coordonnee);
	}
	
	//Gestion des citoyens, utilisation thread
	for(int i=11; i<35; i++) {
		printf(" Creation du medecin %d\n", i-10);
		do {
			coordonnee.x=rand()%7;
			coordonnee.y=rand()%7;
		} while (memoire->matrice[coordonnee.x][coordonnee.y].bat.type != type_maison);
		
		pthread_create(&citoyen[i], NULL, initCitoyen, &coordonnee);
	}
	

	for (int i=0; i<35;i++) {
		pthread_join(citoyen[i], NULL);
	}
	
	
	/*
	* - 5 - ferme le segment de mémoire partagée - Jamais atteint d'où la gestion
	* par interception du signal SIGINT (<CTRL-C>).
	*/
	if (shm_unlink(CHEMIN_MEMOIRE) != 0) {
		traiter_erreur("Erreur lors de l'appel à shm_unlink\n");
	}
	exit(EXIT_SUCCESS);
}
