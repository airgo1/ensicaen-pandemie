/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 
 
 /**
 * @author Burdet Nicolas <burdet@b35pc23.ecole.ensicaen.fr>
 * @version 0.0.1 / 29-05-2018
 */

/**
 * @file citoyen.c
 */
 
#include <epidemie.h>

void *initMedecin(coord cordonnee) {
	long vie = 100;
	int vivant_mort = type_vivant;
	coord position = cordonnee;
	
	printf("\n début du thread medecin\n");
	
	semaphore_t *ville;
	struct Ville *memoire;/* pointeur sur le segment mémoire "projeté" */

	int descripteur_memoire;
	int taille_memoire;

	taille_memoire = (1 * sizeof(struct Ville));

	signal(SIGINT, traiter_sigint);
	
	/* - 1 - crée le segment de mémoire partagée comme un fichier */
	descripteur_memoire = shm_open(CHEMIN_MEMOIRE, O_RDWR, S_IRUSR|S_IWUSR);

	if (descripteur_memoire < 0) {
		traiter_erreur("Erreur lors de l'appel à shm_open\n");
	}

	/* - 3 - projette le segment de mémoire partagée dans l'espace mémoire du ←-processus */
	memoire = (struct memory_t *) mmap(
		NULL,
		taille_memoire,
		PROT_WRITE,
		MAP_SHARED,
		descripteur_memoire,
		0);
		
	memoire->nb_personne++;

	while (memoire->tour != 100 || vivant_mort == type_mort) {
		while (memoire->next_tour!=0);
		memoire->next_tour++;
	}
	
	/*
	* - 5 - ferme le segment de mémoire partagée - Jamais atteint d'où la gestion
	* par interception du signal SIGINT (<CTRL-C>).
	*/
	if (shm_unlink(CHEMIN_MEMOIRE) != 0) {
		traiter_erreur("Erreur lors de l'appel à shm_unlink\n");
	}
	
	printf("\n Fin du thread medecin\n");
	fflush(stdout);

	pthread_exit(NULL);
}

void *initPompier	(coord cordonnee) {
	long vie = 100;
	int vivant_mort = type_vivant;
	coord position = cordonnee;
	
	printf("\n début du thread pompier\n");

	semaphore_t *ville;
	struct Ville *memoire;/* pointeur sur le segment mémoire "projeté" */

	int descripteur_memoire;
	int taille_memoire;

	taille_memoire = (1 * sizeof(struct Ville));

	signal(SIGINT, traiter_sigint);
	
	/* - 1 - crée le segment de mémoire partagée comme un fichier */
	descripteur_memoire = shm_open(CHEMIN_MEMOIRE, O_RDWR, S_IRUSR|S_IWUSR);

	if(descripteur_memoire < 0) {
		traiter_erreur("Erreur lors de l'appel à shm_open\n");
	}

	/* - 3 - projette le segment de mémoire partagée dans l'espace mémoire du ←-processus */
	memoire = (struct memory_t *) mmap(
		NULL,
		taille_memoire,
		PROT_WRITE,
		MAP_SHARED,
		descripteur_memoire,
		0);
		
		memoire->nb_personne++;

	while(memoire->tour != 100 || vivant_mort == type_mort) {
		while (memoire->next_tour!=0);
		memoire->next_tour++;
	}
	
	/*
	* - 5 - ferme le segment de mémoire partagée - Jamais atteint d'où la gestion
	* par interception du signal SIGINT (<CTRL-C>).
	*/
	if (shm_unlink(CHEMIN_MEMOIRE) != 0) {
		traiter_erreur("Erreur lors de l'appel à shm_unlink\n");
	}
	

	printf("\n Fin du thread Pompier\n");
	fflush(stdout);

	pthread_exit(NULL);
}

void *initCitoyen (coord cordonnee) {
	long vie = 100;
	int vivant_mort = type_vivant;
	coord position = cordonnee;
	
	printf("\n début du thread citoyen\n");
	
	semaphore_t *ville;
	struct Ville *memoire;/* pointeur sur le segment mémoire "projeté" */

	int descripteur_memoire;
	int taille_memoire;

	taille_memoire = (1 * sizeof(struct Ville));

	signal(SIGINT, traiter_sigint);
	
	/* - 1 - crée le segment de mémoire partagée comme un fichier */
	descripteur_memoire = shm_open(CHEMIN_MEMOIRE, O_RDWR, S_IRUSR|S_IWUSR);

	if (descripteur_memoire < 0) {
		traiter_erreur("Erreur lors de l'appel à shm_open\n");
	}

	/* - 3 - projette le segment de mémoire partagée dans l'espace mémoire du ←-processus */
	memoire = (struct memory_t *) mmap(
		NULL,
		taille_memoire,
		PROT_WRITE,
		MAP_SHARED,
		descripteur_memoire,
		0);
		
	memoire->nb_personne++;

	while (memoire->tour != 100 || vivant_mort == type_mort) {
		while (memoire->next_tour!=0);
		memoire->next_tour++;
		
	}
	
	/*
	* - 5 - ferme le segment de mémoire partagée - Jamais atteint d'où la gestion
	* par interception du signal SIGINT (<CTRL-C>).
	*/
	if (shm_unlink(CHEMIN_MEMOIRE) != 0) {
		traiter_erreur("Erreur lors de l'appel à shm_unlink\n");
	}
	
	printf("\n Fin du thread citoyen\n");
	fflush(stdout);

	pthread_exit(NULL);
}
