/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 
 
 /**
 * @author Burdet Nicolas <burdet@b35pc23.ecole.ensicaen.fr>
 * @version 0.0.1 / 29-05-2018
 */

/**
 * @file serveur.c
 */
 
#include <epidemie.h>

void initserveur () {
	Ville * Caen;
	int descripteur_memoire;
	int taille_memoire;
	
	int proba;
	int i,j;
	
	taille_memoire = (1 * sizeof(struct Ville));
   
	signal(SIGINT, traiter_sigint); /* préférez la version POSIX vue en cours */
   
	/* crée le segment de mémoire partagée comme un fichier */
	descripteur_memoire = shm_open( CHEMIN_MEMOIRE, 
									O_RDWR | O_CREAT, 
									S_IRUSR|S_IWUSR   );

	if(descripteur_memoire < 0) {
	  traiter_erreur("Erreur lors de l'appel à shm_open\n");
	}
	
	/* configue la taille du segment de mémoire partagée */
	ftruncate(descripteur_memoire, taille_memoire);

	/* projette le segment de mémoire partagée dans l'espace mémoire du processus */
	Caen = (struct Ville *) mmap(
		NULL,
		taille_memoire,
		PROT_READ | PROT_WRITE,
		MAP_SHARED,
		descripteur_memoire,
		0);
		
	if(Caen == MAP_FAILED) {
		traiter_erreur("Erreur lors de l'appel à mmap\n");;
	}
	generateur_aleatoire(Caen);
	
	Caen->affiche = 0;
	
	for(Caen->tour=0;Caen->tour<100; Caen->tour++) {
	
		// On vérifie le niveau de contamination de la case et en fonction de la contanmination des case adjacente, on augmente ou non leur contamination
		for(i=0;i<7;i++) {
			for (j=0;j<7;j++) {
				if(Caen->matrice[i][j].bat.type = 0) {
					
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i+1)%7][(j+1)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i+1)%7][(j+1)%7].bat.type == 0){
							Caen->matrice[(i+1)%7][(j+1)%7].bat.contamination = Caen->matrice[(i+1)%7][(j+1)%7].bat.contamination + rand()%20;
						}
					}
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i-1)%7][(j+1)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i-1)%7][(j+1)%7].bat.type == 0) {
							Caen->matrice[(i-1)%7][(j+1)%7].bat.contamination = Caen->matrice[(i-11)%7][(j+1)%7].bat.contamination + rand()%20;
						}
					}
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i+1)%7][(j)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i+1)%7][(j)%7].bat.type == 0) {
							Caen->matrice[(i+1)%7][(j)%7].bat.contamination = Caen->matrice[(i+1)%7][(j)%7].bat.contamination + rand()%20;
						}
					}
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i)%7][(j+1)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i)%7][(j+1)%7].bat.type == 0) {
							Caen->matrice[(i)%7][(j+1)%7].bat.contamination = Caen->matrice[(i)%7][(j+1)%7].bat.contamination + rand()%20;
						}
					}
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i-1)%7][(j-1)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i-1)%7][(j-1)%7].bat.type == 0) {
							Caen->matrice[(i-1)%7][(j-1)%7].bat.contamination = Caen->matrice[(i-1)%7][(j-1)%7].bat.contamination + rand()%20;
						}
					}
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i-1)%7][(j)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i-1)%7][(j)%7].bat.type == 0) {
							Caen->matrice[(i-1)%7][(j)%7].bat.contamination = Caen->matrice[(i-1)%7][(j)%7].bat.contamination + rand()%20;
						}
					}
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i)%7][(j-1)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i)%7][(j-1)%7].bat.type == 0) {
							Caen->matrice[(i)%7][(j-1)%7].bat.contamination = Caen->matrice[(i)%7][(j-1)%7].bat.contamination + rand()%20;
						}
					}
					if(Caen->matrice[i][j].bat.contamination > Caen->matrice[(i+1)%7][(j-1)%7].bat.contamination) {
						proba = rand()%100;
						if(proba > 15 && Caen->matrice[(i+1)%7][(j-1)%7].bat.type == 0) {
							Caen->matrice[(i+1)%7][(j-1)%7].bat.contamination = Caen->matrice[(i+1)%7][(j-1)%7].bat.contamination + rand()%20;
						}
					}
				}
			}
		}
		
		
		
		printf("fin du tour %d", Caen->tour); 
		
		while(Caen->nb_personne != Caen->next_tour) {
			sleep(1000);
		}
		
		Caen->next_tour==0;
		//On initialise la variable à 1 pour afficher lancer la fonction affiche
		Caen->affiche = 1;
		
		while(Caen->affiche != 0) {
			sleep(1000);
		}
		
		Caen->affiche = 0;
	}
	printf("Fin du serveur\n");

	/*
	* Ferme le segment de mémoire partagée - Jamais atteint d'où la gestion
	* par interception du signal SIGINT (<CTRL-C>).
	*/
	if(shm_unlink(CHEMIN_MEMOIRE) != 0) {
	  traiter_erreur("Erreur lors de l'appel à shm_unlink\n");
	}	
}

void generateur_aleatoire (Ville * Caen) {
	
	int i,j;
	int aleax, aleay;

	for (i=0;i<7;i++) {
		for (j=0;j<7;j++) {
			Caen->matrice[i][j].bat.type = 0;
			Caen->matrice[i][j].bat.contamination = 0;
		}
	}
	
	Caen->matrice[3][3].bat.type =  type_hopital;
	Caen->matrice[0][6].bat.type =  type_caserne;
	Caen->matrice[6][0].bat.type =  type_caserne;
	
	for (i=0;i<12;) {
		aleax = rand()%7;
		aleay = rand()%7;
		
		//printf("valeur alea x = %d \n",aleax);
		//printf("valeur alea y = %d \n",aleay);
		
		if ( Caen->matrice[aleax][aleay].bat.type == 0) {
			Caen->matrice[aleax][aleay].bat.type = type_maison;
			Caen->matrice[aleax][aleay].bat.contamination = 0;
			i++;
		}	
	}
	
	Caen->nb_personne = 0;
	Caen->next_tour   = 0;
	
	for (i=0;i<7;i++) {
		for (j=0;j<7;j++) {
			if (Caen->matrice[i][j].bat.type == 0) {
				Caen->matrice[i][j].bat.contamination = rand()%10;
			}
		}
	}
	
}
