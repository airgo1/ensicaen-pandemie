/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Burdet Nicolas <burdet@b35pc23.ecole.ensicaen.fr>
 * @version 0.0.1 / 29-05-2018
 */

/**
 * @file iplot.h
 */


#ifndef __IPLOT_H
#define __IPLOT_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <semaphore.h>

#define NB_THREADS 50000
#define OK 0

#define INPUT 1
#define OUTPUT 0
#define KEYBOARD 0
#define STOP -1
#define VILLAGE_SIZE sizeof(Village)

#define max_x 				7
#define max_y				7

#define type_maison 		1
#define type_caserne 		2
#define type_hopital		3
#define type_terrain_vague  0

#define type_pompier		2
#define type_medecin		3
#define type_nomale			1

#define max_maison 			6
#define max_caserne 		8
#define max_hopital 		12
#define max_terrain_vague 	16

#define type_vivant 	1
#define type_mort 		0

#define EVER ;;

#define CHEMIN_MEMOIRE "/shmname"

typedef sem_t semaphore_t;

typedef struct Citoyen {
	int type;
	long contamination;
}Citoyen;

typedef struct Citoyenc {
	int type;
	long contamination;
	int x,y;
}Citoyenc;

typedef struct Batiment {
	int type;
	long contamination;
}Batiment;

typedef struct Case {
	Batiment bat;
	Citoyen personne[35];
}Case;

typedef struct Ville {
		Case matrice[7][7];
		int nb_personne;
		int next_tour;
		int tour;
		int affiche;
}Ville;

typedef struct coord {
	int x;
	int y;
}coord;

/*
 * Cette fonction affiche mes erreur qu'il peut y avoir
 * */
void 			traiter_sigint 			(int signum);

/*
 * Cette fonction permet de lancer les diférent procesus pére et fils
 * */
void 			init					(void);

/*
 * 
 *  Cette fonction lance le serveur qui va gérer créer la mémoire partager et gérer la map
 * */
void 			initserveur 			(void);

/*
 * Cette fonction permet de lancer la parti client, qui va initialiser les threads avec les différents citoyens
 * 
 * */
void 			initsimulation 			(void);

/*
 * Cette fonction affiche les messages d'erreur lié au fork
 * 
 * */
void 			handle_fatal_error 		(char *msg);

/*
 * Cette fonction ouvre un semaphore pour lire dans l
 * */
semaphore_t * 	ouvrir_semaphore		(void);

/*
 * 
 * */
void 			p						(semaphore_t * sem);

/*
 * 
 * */
void 			v						(semaphore_t * sem);

/*
 * Cette fonction permet de fermer un semaphore
 * */
void 			fermer_semaphore		(semaphore_t * sem);

/*
 * Cette fonction affiche la carte 
 * */
void			afficher_carte			(void);

/*
 * Cette fonction créer la carte à l'initialisation du serveur
 * */
void 			generateur_aleatoire 	(Ville *Caen);

/*
 * Cette fonction permet de gerer un medecin
 * */
void 			*initMedecin			(coord cordonnee);

/*
 * Cette fonction permet de gerer un mompier
 * */
void 			*initPompier			(coord cordonnee);

/*
 * Cette fonction permet de gerer un citoyen
 * */
void 			*initCitoyen			(coord cordonnee);



#endif
